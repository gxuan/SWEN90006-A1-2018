package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PartitioningTests
{
	
	
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }


  @Test (expected= NoReturnValueException.class) 
  public void EC1Test()
  {
	  String a ="MOV R3 2;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC2Test()
  {
	  String a ="      ;;white space line";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test  
  public void EC3Test()
  {
	  String a ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC4Test()
  {
	  String a ="RET R-1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC5Test()
  {
	  String a ="ADD R1 R2 R3;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC6Test()
  {
	  String a ="ADD R1 R2 R33;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC7Test()
  {
	  String a ="ADD R1 R35 R2;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC8Test()
  {
	  String a ="ADD R38 R2 R3;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC9Test()
  {
	  String a ="LDR R1 R2 65533;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC10Test()
  {
	  String a ="LDR R1 R2 65536;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC11Test()
  {
	  String a ="LDR R3 R255 65532;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC12Test()
  {
	  String a ="LDR R39 R2 65532;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC13Test()
  {
	  String a ="STR R1 65533 R3;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC14Test()
  {
	  String a ="STR R1 -65533 R36;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC15Test()
  {
	  String a ="STR R1 65536 R3;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC16Test()
  {
	  String a ="STR R111 65530 R3;";
	  String b ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC18Test()
  {
	  String a ="JMP 2;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test (expected= NoReturnValueException.class) 
  public void EC19Test()
  {
	  String a ="JMP 4;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC20Test()
  {
	  String a ="JMP 65536;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC22Test()
  {
	  String a ="JZ R1 2;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test (expected= NoReturnValueException.class) 
  public void EC23Test()
  {
	  String a ="JZ R1 4;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC24Test()
  {
	  String e ="MOV R10 10";
	  String a ="JZ R10 5;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(e);
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC25Test()
  {
	  String a ="JZ R1 65537;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC26Test()
  {
	  String a ="JZ R35 65531;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC27Test()
  {
	  String a ="MOV R-2 5;";
	  String b ="RET 2";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  @Test  
  public void EC28Test()
  {
	  String e ="MOV R15 150";
	  String a ="RET R15";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(e);
	  lines.add(a);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 150);
    
  }
  
  @Test (expected= InvalidInstructionException.class) 
  public void EC29Test()
  {
	  String a ="MOV R20 -65536;";
	  String b ="RET R20";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
      Machine m = new Machine();
      m.execute(lines);
    
  }
  
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
