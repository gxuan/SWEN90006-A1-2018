package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  @Test  
  public void Boundary1Test() //on point
  {
	  String a ="RET R0;";
	  String b ="RET R31;";
	  String c ="RET R1;";
	  String d ="RET R30;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
 
  @Test  
  public void Boundary2Test()
  {
	  String a ="ADD R1 R2 R0;";
	  String b ="ADD R1 R2 R31;";
	  String c ="ADD R1 R2 R1;";
	  String d ="ADD R1 R2 R30;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary3Test()
  {
	  String a ="ADD R1 R0 R0;";
	  String b ="ADD R1 R31 R31;";
	  String c ="ADD R1 R1 R1;";
	  String d ="ADD R1 R30 R30;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary4Test()
  {
	  String a ="ADD R0 R0 R0;";
	  String b ="ADD R31 R31 R31;";
	  String c ="ADD R1 R1 R1;";
	  String d ="ADD R30 R30 R30;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary5Test()
  {
	  String a ="LDR R1 R2 65535;";
	  String b ="LDR R1 R2 -65535;";
	  String c ="LDR R1 R2 -65534;";
	  String d ="LDR R1 R2 65534;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary6Test()
  {
	  String a ="LDR R1 R0 65535;";
	  String b ="LDR R1 R31 -65535;";
	  String c ="LDR R1 R1 -65534;";
	  String d ="LDR R1 R30 65534;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary7Test()
  {
	  String a ="LDR R0 R0 65535;";
	  String b ="LDR R31 R31 -65535;";
	  String c ="LDR R1 R1 -65534;";
	  String d ="LDR R30 R30 65534;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary8Test()
  {
	  String a ="STR R1 -65533 R0;";
	  String b ="STR R1 -65533 R31;";
	  String c ="STR R1 -65533 R1;";
	  String d ="STR R1 -65533 R30;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary9Test()
  {
	  String a ="STR R1 -65535 R0;";
	  String b ="STR R1 65535 R31;";
	  String c ="STR R1 -65534 R1;";
	  String d ="STR R1 65534 R30;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary10Test()
  {
	  String a ="STR R0 -65535 R0;";
	  String b ="STR R31 65535 R31;";
	  String c ="STR R1 -65534 R1;";
	  String d ="STR R30 65534 R30;";
	  String e ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  
  @Test  
  public void Boundary11Test()
  {
	  
	  String a ="JMP 3;";
	  String b ="JMP 2;";
	  //since if JMP is negative, there may be a infinite loop,
	  //so will not test this point
	  String c ="ADD R1 R2 R3";
	  String d ="ADD R4 R5 R6;";
	  String e ="ADD R1 R2 R3";
	  String f ="ADD R4 R5 R6;";
	  String g ="RET R1;";
	  String h ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  lines.add(c);
	  lines.add(d);
	  lines.add(a);
	  lines.add(e);
	  lines.add(f);
	  lines.add(g);
	  lines.add(h);  
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(c);
	  lines2.add(d);
	  lines2.add(b);
	  lines2.add(e);
	  lines2.add(f);
	  lines2.add(g);
	  lines2.add(h);  
	  Machine m1 = new Machine();
      assertEquals(m1.execute(lines2), 0);
  }
  
  @Test  
  public void Boundary12Test()
  {
	  
	  String a ="JMP 1;";
	  //since if JMP==0 or JMP is negative, there may be a infinite loop,
	  //so will not test this point
	  String c ="ADD R1 R2 R3";
	  String d ="ADD R4 R5 R6;";
	  String e ="ADD R1 R2 R3";
	  String f ="ADD R4 R5 R6;";
	  String g ="RET R1;";
	  String h ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(c);
	  lines.add(d);
	  lines.add(a);
	  lines.add(e);
	  lines.add(f);
	  lines.add(g);
	  lines.add(h);  
	  Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
	  
      
  }
  
  @Test  
  public void Boundary14Test()
  {
	  String a ="JZ R1 3;";
	  String e ="JZ R1 2;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines1 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines1.add(e);
	  lines1.add(b);
	  lines1.add(c);
	  lines1.add(d);
      Machine m1 = new Machine();
      assertEquals(m1.execute(lines), 0);
    
  }
  
  @Test  
  public void Boundary15Test()
  {
	  String a ="JZ R1 1;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      
    
  }
  
  public void Boundary16Test()
  {
	  String a ="JZ R5 3;";
	  String e ="MOV R5 -1";
	  String f ="MOV R5 1";
	  String b ="MOV R3 3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R3;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines1 =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 3);
      lines1.add(e);
      lines1.add(a);
	  lines1.add(b);
	  lines1.add(c);
	  lines1.add(d);
      Machine m1 = new Machine();
      assertEquals(m1.execute(lines), 0);
      lines2.add(f);
      lines2.add(a);
	  lines2.add(b);
	  lines2.add(c);
	  lines2.add(d);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines), 0);
    
  }
  
  @Test  
  public void Boundary18Test()
  {
	  String a ="JZ R0 1;";
	  String e ="JZ R31 1;";
	  String f ="JZ R1 1;";
	  String g ="JZ R30 1;";
	  String b ="ADD R1 R2 R3";
	  String c ="ADD R4 R5 R6;";
	  String d ="RET R1;";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(b);
	  lines.add(c);
	  lines.add(d);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(e);
      lines2.add(b);
	  lines2.add(c);
	  lines2.add(d);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 0);
      lines3.add(f);
      lines3.add(b);
	  lines3.add(c);
	  lines3.add(d);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), 0);
      lines4.add(g);
      lines4.add(b);
	  lines4.add(c);
	  lines4.add(d);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
      
    
  }
  
  @Test  
  public void Boundary19Test() //on point
  {
	  String a ="MOV R25 -65535;";
	  String b ="MOV R25 65535;";
	  String c ="MOV R25 -65534;";
	  String d ="MOV R25 65534;";
	  String e ="RET R25";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), -65535);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2), 65535);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3), -65534);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 65534);
    
  }
  
  @Test  
  public void Boundary20Test() //on point
  {
	  String a ="MOV R0 0;";
	  String b ="MOV R31 0;";
	  String c ="MOV R1 0;";
	  String d ="MOV R30 0;";
	  String e ="RET R0";
	  final List<String> lines =new ArrayList<String>();
	  final List<String> lines2 =new ArrayList<String>();
	  final List<String> lines3 =new ArrayList<String>();
	  final List<String> lines4 =new ArrayList<String>();
	  lines.add(a);
	  lines.add(e);
      Machine m = new Machine();
      assertEquals(m.execute(lines), 0);
      lines2.add(b);
      lines2.add(e);
      Machine m2 = new Machine();
      assertEquals(m2.execute(lines2),  0);
      lines3.add(c);
      lines3.add(e);
      Machine m3 = new Machine();
      assertEquals(m3.execute(lines3),  0);
      lines4.add(d);
      lines4.add(e);
      Machine m4 = new Machine();
      assertEquals(m4.execute(lines4), 0);
    
  }
  

  
  
  

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
